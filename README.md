dxarts461 : Read Me
========================
_DXARTS 461 (Winter 202x): Introduction to digital sound synthesis._

&nbsp;

&nbsp;

Installing
==========

Distributed via [DXARTS 461 Tutorials](https://gitlab.com/dxarts/classes/dxarts46x/dxarts461a_wi2x.quark).

Start by reviewing the Quark installation instructions
[found here](https://github.com/supercollider-quarks/quarks#installing). See
also [Using Quarks](http://doc.sccode.org/Guides/UsingQuarks.html).

With [git](https://git-scm.com/) installed, you can easily install the
[DXARTS 461 Tutorials](https://gitlab.com/dxarts/classes/dxarts46x/dxarts461a_wi2x.quark)
directly by running the following line of code in SuperCollider:

    Quarks.install("https://gitlab.com/dxarts/classes/dxarts46x/dxarts461a_wi2x.quark");



Feedback and Bug Reports
========================

Known issues are logged at
[GitLab](https://gitlab.com/dxarts/classes/dxarts46x/dxarts461a_wi2x.quark/-/issues).

&nbsp;

&nbsp;



List of Changes
---------------

Version 23.3.0

* Winter 2023: week 3
    * fix: 03b mismatched -sineFill, -waveFill skiptimes

Version 23.0.0

* Winter 2023: week 0
    * fix: 01a broken links: Stelios Manousakis, Juan Pampin
    * fix: issue #20
    * update: boilerplate
    * update Quark & README
        * link to briefs 2023

Version 22.0.0

* Winter 2022: week 1
    * update: MyPlan link
    * update: boilerplate Canvas link
    * fix: misc broken links & misnamed files
    * refactor: clarify environment vs interpreter vars
    * refactor: replace interpreter w/ environment vars
    * refactor: increase code verbosity in places
    * refactor: include mention of MainParadigm

Version 21.4.0

* Winter 2021: week 4
    * refactor: 03a, replace arg ...; with ||
    * fix: 04b, phase = pi/2

Version 21.1.0

* Winter 2021: week 1

Version Pre-release

* Pre-release.


&nbsp;

&nbsp;

Authors
=======

&nbsp;

Copyright Joseph Anderson and the DXARTS Community, 2012-2023.

[Department of Digital Arts and Experimental Media (DXARTS)](https://dxarts.washington.edu/)
University of Washington

&nbsp;


Contributors
------------

Version 21.1.0 - 23.3.0
*  Joseph Anderson (@joslloand)

Version pre-release
*  Joseph Anderson (@joslloand)
*  Juan Pampin (@jpampin)
*  Joshua Parmenter (@joshpar)
*  Daniel Peterson (@dmartinp)
*  Stelios Manousakis (@Stylianos91)
*  James Wenlock (@wenloj)


Contribute
----------

As part of the wider SuperCollider community codebase, contributors are encouraged to observe the published SuperCollider guidelines found [here](https://github.com/supercollider/supercollider#contribute).


License
=======

The [DXARTS 461 Tutorials](https://gitlab.com/dxarts/classes/dxarts46x/dxarts461a_wi2x.quark) is free software available under [Version 3 of the GNU General Public License](https://www.gnu.org/licenses/gpl-3.0.en.html). See [LICENSE](LICENSE) for details.
