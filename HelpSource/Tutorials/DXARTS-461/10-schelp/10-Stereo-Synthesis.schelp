title:: 10. Stereo Image Synthesis
summary:: DXARTS 461 - Week 10
categories:: Tutorials>DXARTS>461
keyword:: DXARTS

link::Tutorials/DXARTS-461/09-schelp/09c-SS-&-FIR-Filtering## << 09c. Subtractive Synthesis - Measured Spectra & FIR Filtering::

In Week 2 we introduced two channel stereophonic link::Tutorials/DXARTS-461/02-schelp/02e-Visualization-Panorama-Acknowledge#Panorama#panorama::, aka emphasis::panning::. For today's session we'll explore a wider range of techniques to synthesize stereophonic emphasis::imaging::.

____________________________________

Let's start by booting the server:

code::
s.boot
::

Opening the scopes:

code::
// display
(
var numChannels = 1;

Stethoscope.new(s, numChannels);
Stethoscope.new.style_(2);  // And another scope, in lissajou mode
/*
Grab and rearrange the two Stethoscopes so you can see both of them.
*/

FreqScope.new;
)
::

And making the associated link::Classes/MainParadigm:: projects:

code::
(
~tutorialName = "10-Stereo-Synthesis";  // this tutorial page name
~courseQuark = "dxarts461a_wi2x";  // the course name

// make the project
MainParadigm.new46xExamples(~tutorialName, ~courseQuark)
)
::


section::Differences between channels

Two channel stereophonic footnote::emphasis::Stereophonic:: means "solid" and refers to the synthesis of emphasis::phantom images:: between loudspeakers. Systems with more than two loudspeakers are also described as emphasis::stereophonic:: if emphasis::phantom images:: are synthesized.:: images are synthesized as a result of differences in the signals fed to both channels. If there are emphasis::no:: differences the synthesized emphasis::phantom image:: appears centered between the two loudspeakers, on the "surface" of the speakers. The synthesized differences can be emphasis::amplitude:: or emphasis::phase:: or even emphasis::time:: footnote::While emphasis::phase:: and emphasis::time:: are closely related, when speaking of emphasis::time:: difference, we're usually describing differences in the time delay of amplitude envelopes. link::https://en.wikipedia.org/wiki/Group_delay_and_phase_delay##Group delay:: is the technical description of delay of amplitude envelopes. link::https://en.wikipedia.org/wiki/Interaural_time_difference##Interaural Time Difference (ITD):: is the arrival time difference in amplitude envelope between the ears.:: differences, or all of these.


section::Amplitude differences

In Week 2 we saw the use of the link::Tutorials/DXARTS-461/02-schelp/02e-Visualization-Panorama-Acknowledge#Square%20root%20panning%20law#square root panning law:: and the link::Tutorials/DXARTS-461/02-schelp/02e-Visualization-Panorama-Acknowledge#Sine-cosine%20panning%20law#sine-cosine panning law::, implementing these as functions returning scaling coefficients. We should be well familiar with this technique, as we've been using it throughout the Quarter. You'll recall I described my preference is to use latter:

image::10_eq01.png::


subsection:: Sine-cosine law via LFO modulation


For all the following examples, observe the difference between the left and right channels in the link::Classes/Stethoscope::. Use your ears, too, auditioning how the stereo image broadens or is otherwise modulated!!


Let's code up a simple example illustrating one reason:

code::
(
{
    var freq, gain, amp, autoPanRate;
	var quadPhase;
    var sinCosPanLaw, saw, out;

    // params
    freq = 220.0;
    gain = -12;
    autoPanRate = 0.5;

	// "settings"
	quadPhase = pi/4 * [ 3, 1 ];  // phase difference of pi/2

    // calcs
    amp = gain.dbamp;

	// panning law - as a low frequency modulator (RM)
    sinCosPanLaw = SinOsc.ar(
		autoPanRate / 2,
		quadPhase
	);

    // synth
    saw = Saw.ar(freq, amp);

	// pan (RM in quadrature) here!
    out = sinCosPanLaw * saw;

    out;
}.play;  // what's happening here?
)
/*
Stop synthesis via cmd-.
*/
::

We're using link::Classes/SinOsc:: and then evaluating the sine-cosine panning law by link::Tutorials/DXARTS-461/04-schelp/04b-RM(DSB)-SSB-Modulation#Ring Modulation%20(RM),%20Balanced%20Modulation%20(BM),%20and%20Double%20Sideband%20Modulation%20(DSB)#Ring Modulating:: in link::https://en.wikipedia.org/wiki/In-phase_and_quadrature_components##quadrature::. Doing so gives us an "auto-panner". The emphasis::auto pan rate:: is set to a fixed value, but there's no reason this can't be enveloped.

subsection:: Sine-cosine law via evaluation

Here's another implementation:

code::
(
{
    var freq, gain, amp, autoPanRate, panAngle;
    var sinCosPanLaw, saw, out;

    // params
    freq = 220.0;
    gain = -12;
    autoPanRate = 0.5;

    // calcs
    amp = gain.dbamp;


    // panning angle & law
    panAngle = LFNoise2.ar(autoPanRate, pi);
    sinCosPanLaw = [ panAngle.cos, panAngle.sin ];

    // synth
    saw = Saw.ar(freq, amp);

    // pan here!
    out = sinCosPanLaw * saw;

    out;
}.play;  // what's happening here?
)
/*
Stop synthesis via cmd-.
*/
::

Here we're using link::Classes/LFNoise2:: to synthesize a randomly modulating panorama angle. The sine-cosine law is being evaluated directly.

We could bring these two implementation together. Can you see how?

section::Phase differences

Differences in phase between the two channels usually result in synthesizing a perception of image "breadth" or "depth".


subsection::Periodic signals

In discussing link::Tutorials/DXARTS-461/05-schelp/05-FM-Modulation#Simple%20FM#Frequency Modulation::, we briefly mentioned that link::Tutorials/DXARTS-461/05-schelp/05-FM-Modulation#footnote_10#Frequency and Phase Modulation are closely related::. In the examples immediately below, we touch the emphasis::frequency:: argument.

Here's an example illustrating how changing phase relationships between the two stereo channels create a modulating spatial impression:

code::
(
{
	var freq, gain, amp, phaseRate, freqOffset;
	var out;

	// params
	freq = 220.0;
	gain = -12;
	phaseRate = 0.5;

	// calcs
	amp = gain.dbamp;
	freqOffset = phaseRate/2 * [1, -1];

	// synth
	out = Saw.ar(freq + freqOffset, amp);

	out;
}.play;  // what's happening here?
)
/*
Stop synthesis via cmd-.
*/
::

An interesting example using link::https://en.wikipedia.org/wiki/Stereophonic_sound#M/S_technique:_mid/side_stereophony#Middle and Side theory:::

code::
(
{
	var freq, gain, amp, phaseRate, freqOffset;
	var out;

	// params
	freq = 220.0;
	gain = -12;
	phaseRate = 0.5;

	// calcs
	amp = gain.dbamp;
	freqOffset = phaseRate/2 * [1, -1];

	// synth
	out = Saw.ar(freq + freqOffset, amp);
	out = Mix.ar(2.sqrt.reciprocal * [[1, 1], [1, -1]] * out);

	out;
}.play;  // what's happening here?
)
/*
Stop synthesis via cmd-.
*/
::

Another example, similar to the first, but where the phase "drifts" between the two channels:

code::
(
{
	var freq, gain, amp, phaseRate, modRate, freqOffset;
	var out;

	// params
	freq = 220.0;
	gain = -12;
	phaseRate = 0.5;
	modRate = 0.5;

	// calcs
	amp = gain.dbamp;
	freqOffset = phaseRate/2 * [1, -1];

	// synth
	freqOffset = LFNoise2.ar(modRate, freqOffset);
	out = Saw.ar(freq + freqOffset, amp);

	out;
}.play;  // what's happening here?
)
/*
Stop synthesis via cmd-.
*/
::

The broadest image results when the signal on the left and the right are in link::https://en.wikipedia.org/wiki/In-phase_and_quadrature_components##quadrature:::

code::
(
{
	var freq, gain, amp;
	var phase;
	var out;

	// params
	freq = 220.0;
	gain = -12;

	// calcs
	amp = gain.dbamp;
	phase = [pi/2, 0.0];  // quadrature phase!!

	// synth
	out = SinOsc.ar(freq, phase, amp);

	out;
}.play;  // what's happening here?
)
/*
Stop synthesis via cmd-.
*/
::

Given the above just returns a link::https://en.wikipedia.org/wiki/In-phase_and_quadrature_components##quadrature:: sinusoid pair, we probably won't actually have a significant impression of breadth. footnote::Sinsusoids are notoriously difficult to localize.::

note::
Used as the emphasis::basis functions:: for Additive Synthesis, we will hear a distinct broadening of the synthesized image.

Consider updating the UGen graphs you use for Additive Synthesis to use oscillators with phase in quadrature.

Make a new UGen graph called code::sinOscQuadLinenPan::.
::


Let's try Additive Synthesis with drifting phase between left and right channels. Two oscillators are modulated by link::Classes/LFNoise2:: so that the phase "drifts" between them:

code::
/*
Two Sine Oscillators - with drifting phase between them
*/
{| dur, gain = -12.0, ris = 0.1, dec = 0.1, freq = 440.0, driftRate = 0.5, panAngle = 0.0|

	// variables
	var bus;          // var to specify output bus
	var osc, out;     // vars assigned to audio signals
	var amp, phase;  // a few vars for synthesis
	var ampEnv, env;       // vars for envelope signal

	// assign values
	bus = 0;          // first output

	// calcs
	amp = gain.dbamp; // convert from gain in dB to linear amplitude scale

	// the amplitude envelope
	env = Env.linen(ris, 1.0 - (ris + dec), dec);

	// the UGen that synthesises the envelope
	ampEnv = EnvGen.kr(env, timeScale: dur);


	// phase modulator
    phase = LFNoise2.ar(driftRate, pi);
    phase = phase/2 * [1, -1];  // split into two offsets

	// the oscillator
	osc = SinOsc.ar(freq, phase, amp);

	// rescale osc, by multiplying by ampEnv
	osc = ampEnv * osc;

	// expand to two channels - panning
	out = main.functions[\sinCosPanLaw].value(panAngle) * osc;  // <-- Panning happens here!

	// out!!
	Out.ar(bus, out)
}
::

Let's render some examples:

code::
(
~tutorialName = "10-Stereo-Synthesis";  // this tutorial page name
~exampleName = "01-periodic-signals";

Document.open(
	"~/Desktop/%/%/%.scd".format(
		~tutorialName.toLower,
		~exampleName,
		~exampleName
	).standardizePath
)
)
::

Evaluate the codeblock in this open file.

This example offers an Additive Synthesis example similar one we reviewd in link::Tutorials/DXARTS-461/03-schelp/03a-Additive-Synthesis#Scheduling%20Function,%20multiple%20parameters#Week 3::. The only difference is that our Basis Signal, as single link::Classes/SinOsc:: panned between left and right, has been replaced by a pair of link::Classes/SinOsc:: with randomly modulating phase differences between them.

Audition both this example, and Week 3's teletype::03-scheduling-function-multiple-parameters:: example.

Observe the two link::Classes/Stethoscope:: windows.


note::
You can easily update your previous Additive Synthesis designs by calling code::\sinOscDriftLinenPan:: instead of code::\sinOscLinenPan::!!
::


subsection::Stocastic signals

SuperCollider's stochastic generators offer an easy and convenient way to generate signals with the same spectral envelope, but different phases. The link::Tutorials/DXARTS-461/09-schelp/09a-SS-Complex-Sources#Colored%20Noise#colored noise:: generators return emphasis::decorrelated:: signals given multiple code::mul:: arguments when expanded via link::Guides/Multichannel-Expansion##Multichannel Expansion::.

Here's link::Classes/WhiteNoise:: expanding:

code::
(
{
	var gain, amp;
	var out;

	// params
	gain = -12;

	// calcs
	amp = gain.dbamp;


	// synth
	out = WhiteNoise.ar(amp * [1, 1]);  // decorrelated
	// out = WhiteNoise.ar(amp) * [1, 1];  // mono!

	// "panning"
	out = 2.sqrt.reciprocal * out;  // scale to sine-cosine law

	out;
}.play;  // what's happening here?
)
/*
Stop synthesis via cmd-.
*/
::

Obviously, we can keep this in mind when we consider using link::Tutorials/DXARTS-461/09-schelp/09a-SS-Complex-Sources#Colored%20Noise#colored noise:: in conjunction with Subtractive Synthesis:

code::
(
{
	var freq, gain, amp, quality;
	var rq, out;

	// params
	freq = 220.0;
	gain = -12;
	quality = 50.0;

	// calcs
	amp = gain.dbamp;
	rq = quality.reciprocal;


	// synth
	out = WhiteNoise.ar(amp * [1, 1]);  // decorrelated
	// out = WhiteNoise.ar(amp) * [1, 1];  // mono!
	out = BPF.ar(out, freq, rq, quality);

	// "panning"
	out = 2.sqrt.reciprocal * out;  // scale to sine-cosine law

	out;
}.play;  // what's happening here?
)
/*
Stop synthesis via cmd-.
*/
::

SuperCollider's link::Tutorials/DXARTS-461/06-schelp/06-Modulation-&-BLN#Low-pass%20Noise#Sample & Hold Noise generators:: behave slightly differently. To return emphasis::decorrelated:: signals, multiple code::freq:: arguments are expanded:

code::
(
{
	var freq, gain, amp;
	var out;

	// params
	freq = 880.0;
	gain = -3;

	// calcs
	amp = gain.dbamp;


	// synth
	out = LFNoise2.ar(freq * [1, 1], amp);  // decorrelated
	// out = LFNoise2.ar(freq, amp * [1, 1]);  // mono!

	// "panning"
	out = 2.sqrt.reciprocal * out;  // scale to sine-cosine law

	out;
}.play;  // what's happening here?
)
/*
Stop synthesis via cmd-.
*/
::

Clearly, these insights can be applied to the designs seen link::Tutorials/DXARTS-461/06-schelp/06-Modulation-&-BLN#RM%20S&H%20Noise#here:: and link::Tutorials/DXARTS-461/06-schelp/06-Modulation-&-BLN#AM%20S&H%20Noise#here:: to return Band-Limited Noise with spatial breadth.


section:: Time differences

In discussing emphasis::time differences:: between channels, it might help to imagine the signals as two copies of the same emphasis::wavepacket::, each arriving at a different time. Differences in arrival times do result in link::#Phase differences#phase differences:: in the emphasis::steady state:: portion of the signal. The amplitude envelopes also differ in time. You can think of the envelopes as packaging up the waveform in time.

Let's introduce emphasis::Tdelta:: to describe the difference in envelope arrival times between the left and right channels:

image::10_eq02.png::

Where emphasis::TL:: and emphasis::TR:: are the arrival times for left and right.

If the gain for the left and right signals are the same and emphasis::TL:: equals emphasis::TR::, we'll synthesize a phantom image in the center. If the arrival time for the left, emphasis::TL::, leads that for the right, emphasis::TR::, the image will appear towards the left. Similarly, if emphasis::TL:: lags emphasis::TR::, the image appears towards the right.

subsection:: Sine law

The problem of defining appropriate time difference panorama laws for two channel loudspeaker stereo is actually a complex task, involving a number of interacting factors. footnote::Hence, the popularity of link::#Amplitude differences#intensity panning::.:: Perception of imaging varies with frequency emphasis::and:: envelope, among other things.

That said, F. Richard Moore does offer the sine time difference panorama law, footnote::Moore, F. Richard. Elements of Computer Music. Englewood Cliffs, N.J.: Prentice Hall, 1990. See pages 360-362. :: which is suitable for broadband signals. The law is used to calculate emphasis::Tdelta::, the time difference between the two channels. I've mapped the emphasis::panorama angle:: to be the same as we use in link::#Amplitude differences#amplitude panning:::

image::10_eq03.png::

emphasis::Tmax:: is the maximum amount of delay between the two channels. Various authors suggest this should be between 0.6 and 1.0 milliseconds.

If we "split" the arrival time difference between the left and right, we can express the law as:

image::10_eq04.png::

The above gives us the simplest form of the sine time difference law, and allows us to calculate the required lead or lag for the left and right channels. OK, so now we clearly see that "negative time" turns up in our panning law. What is this? Actually, "negative time" means "lead", and "positive time" means "lag". These are the time offsets "added" to the nominal arrival time.

There is a problem for synthesis, however. How can we synthesize a note event "before" the event begins, i.e., in "negative time"? One answer is to slide everything "forward" in time, adding a lag (delay) to emphasis::TL:: and emphasis::TR::. This makes both values always have "positive time" offsets:

image::10_eq05.png::

Interestingly enough, the law is now expressed in terms of link::https://en.wikipedia.org/wiki/Versine#Definitions#hacoversine & hacovercosine:: scaled by emphasis::Tmax::, which does offer a certain elegance.

There is one thing, though, that we might not feel is so beautiful. If the signal is to be synthesized in the center, there will be a delay of one half emphasis::Tmax:: seconds. In most cases this delay won't be objectionable or even audible.

We can, though, re-write the law to normalize out unnecessary lags:

image::10_eq06.png::

Implemented as a function:

code::
// ---- sine
// advocated by Moore

// a function to return time delays for Sine Time Difference Panning Law
// position argument in radians
~sinTDiffLaw = { |angle = 0, maxDelay = 0.0006|
    var theta, sinTheta;

    theta = 2 * angle;
	sinTheta = theta.sin;

	maxDelay/2  * ([-1, 1] * sinTheta + sinTheta.abs)
};


// center
~sinTDiffLaw.value(angle: 0)

// left
~sinTDiffLaw.value(angle: pi/4)

// right
~sinTDiffLaw.value(angle: -pi/4)

// half-way right
~sinTDiffLaw.value(angle: -pi/8)
::


subsection:: Phase & time

Given the delay values for left and right calculated by the sine time difference panorama law, we could just synthesize a signal and then sent the result to a signal processing network with two delay lines, one for each channel. While a convenient solution, we'll regard such a design as more appropriate for emphasis::DXARTS 462: Digital Sound Processing::.

Instead, let's review how to map emphasis::time differences:: to emphasis::phase differences:: when directly synthesizing periodic signals. Recall that the emphasis::wavelength:: of a signal is measured in seconds, and is the reciprocal of emphasis::frequency:::

image::10_eq07.png::

We can view a emphasis::phase offset:: as a emphasis::time delay:::

image::10_eq08.png::

Rearranging the expression allows us to consider a emphasis::time delay:: as a emphasis::phase offset:::

image::10_eq09.png::

This expression expresses: a periodic signal at a given emphasis::frequency:: with a given emphasis::lag in time:: is equivalent to a periodic signal at a given emphasis::frequency:: with a given emphasis::phase offset::. footnote::To be emphasis::absolutely:: true, both signals must be steady-state.::

As the signal is question is periodic, we can apply the link::https://en.wikipedia.org/wiki/Modulo_operation##modulo:: operation to find the equivalent phase offset between code::0:: and code::2pi:: radians:

image::10_eq10.png::

Let's implement this:

code::
// ---- convert delay to phase
~delayToPhase = { |delay, freq = 440.0|
	(-2pi * freq * delay).mod(2pi)
};

// test
~delayToPhase.value(delay: 0.0, freq: 220.0).raddeg
~delayToPhase.value(delay: 1/220.0, freq: 220.0).raddeg
~delayToPhase.value(delay: 1/220.0 * 1/4, freq: 220.0).raddeg
~delayToPhase.value(delay: 1/220.0 * 1/2, freq: 220.0).raddeg
~delayToPhase.value(delay: 1/220.0 * 3/4, freq: 220.0).raddeg
::

Bringing these two functions together allows us to see how to find the emphasis::phase offset:: given a emphasis::panorama angle:: and a emphasis::frequency:::

code::
// test -- find phase offset for 220 Hz
// center
~delayToPhase.value(delay: ~sinTDiffLaw.value(angle: 0), freq: 220.0).raddeg

// left
~delayToPhase.value(delay: ~sinTDiffLaw.value(angle: pi/4), freq: 220.0).raddeg

// right
~delayToPhase.value(delay: ~sinTDiffLaw.value(angle: -pi/4), freq: 220.0).raddeg

// half-way right
~delayToPhase.value(delay: ~sinTDiffLaw.value(angle: -pi/8), freq: 220.0).raddeg
::


subsection:: Envelope delay

Synthesizing envelope delay with SuperCollider is relatively trivial as the link::Classes/Env:: class includes the instance method link::Classes/Env#-delay::.

code::
// ----- envelope delay example
(
var ris, dur;
var angle, maxDelay;

var delays, envs;

// set params
ris = 0.1;  // seconds rather than envelope fraction
dur = 2.0;  // seconds
angle = pi/4;  // hard left
maxDelay = 0.1;  // artificially high value, for visualization

// calculate delays & releases
delays = ~sinTDiffLaw.value(angle, maxDelay);

// generate envelopes
envs = Env.perc(
    ris/dur,
    1.0 - ((ris + delays)/dur)
).delay(delays/dur);


// plot
envs.plot(name: "envelopes");

// synthesize envelopes
{ EnvGen.ar(envs, timeScale: dur) }.plot(dur);
)
::

Notice that we must make sure the envelope values are scaled by code::dur:: if we wish to return the requested envelop delay.

note::The envelope must be synthesized at the audio rate. Use link::Classes/EnvGen#*ar:: to do so!::

subsection::Time difference panorama

Let's examing how time difference panorama may be implemented. Notice how the various aspects of this panning law are coded up across several lines:

code::
/*
A Sine Oscillator - Time difference panorama
*/

{| dur, gain = -12.0, ris = 0.001, freq = 440.0, panAngle = 0.0, maxDelay = 0.0006|

	// variables
	var bus;          // var to specify output bus
	var osc, out;     // vars assigned to audio signals
	var amp, phase;  // a few vars for synthesis
	var delays;
	var ampEnv, env;       // vars for envelope signal

	// assign values
	bus = 0;          // first output

	// calcs
	amp = gain.dbamp; // convert from gain in dB to linear amplitude scale
	delays = main.functions[\sinTDiffLaw].value(panAngle, maxDelay);  // envelope delays  <--- part of the panning
	phase = main.functions[\delayToPhase].value(delays, freq);  // phases of oscillators  <--- part of the panning

	// the amplitude envelopes  <--- part of the panning
	env = Env.perc(ris/dur, 1.0 - ((ris + delays)/dur)).delay(delays/dur);

	// the UGen that synthesises the envelopes
	ampEnv = EnvGen.ar(env, timeScale: dur);  // envelope generated at *ar rate  <--- part of the panning


	// the oscillators
	osc = SinOsc.ar(freq, phase, amp);  //   <--- part of the panning (phase)

	// rescale osc, by multiplying by ampEnv    <--- part of the panning
	out = ampEnv * osc;

	// out!!
	Out.ar(bus, out)
}
::

Let's render:

code::
(
~tutorialName = "10-Stereo-Synthesis";  // this tutorial page name
~exampleName = "02-time-difference-panorama";

Document.open(
	"~/Desktop/%/%/%.scd".format(
		~tutorialName.toLower,
		~exampleName,
		~exampleName
	).standardizePath
)
)
::

Evaluate the codeblock in this open file.

Auditioning over headphones, you'll likely notice a vivid and strong effect.

How does this example sound over loudspeakers?


section:: Further Help

list::
## link::Help::
::
