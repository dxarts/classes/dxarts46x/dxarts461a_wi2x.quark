/*
Title: 04-a-simple-example

Course: DXARTS 461a Winter 2023 (https://canvas.uw.edu/courses/1612218)
Authors: Joseph Anderson, Dan Peterson, James Wenlock, et al.
Affiliation: DXARTS, University of Washington (https://dxarts.washington.edu/)
License: GPLv3

*/

/*
Main Paradigm example:

03a-Additive-Synthesis
-->A simple example


To render, evaluate the code block found in "04-a-simple-example.scd".

*/