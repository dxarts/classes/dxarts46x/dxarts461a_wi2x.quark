/*
Course: DXARTS 461a Winter 2023 (https://canvas.uw.edu/courses/1612218)
Authors: Joseph Anderson, Dan Peterson, James Wenlock, et al.
Affiliation: DXARTS, University of Washington (https://dxarts.washington.edu/)
License: GPLv3

function to call \additiveFunc

ten tones from a chromatic (12-tet) scale, starting from freq
*/

({ |main, start, dur, gain = -12.0, ris = 0.1, dec = 0.1, freq = 440.0, partialData|

	var intervals = 10.collect({ |i|
		2.pow( i / 12)
	});

	// iterate through the intervals array, call \additiveFunc to add notes to the score
	intervals.do({ |interval, i|
		var thisFreq ;
		var thisStart;

		thisFreq = freq * interval;
		thisStart = start + (i * dur);

		// evaluate the additive synthesis function
		// args: start, dur, gain, ris, dec, freq, partialGains
		// this function adds individual partials to the score to be played
		main.functions[\additiveFunc].value(
			main: main,
			start: thisStart,
			dur: dur,
			gain: gain,
			ris: ris,
			dec: dec,
			freq: thisFreq,
			partialData: partialData
		)
	})
})
