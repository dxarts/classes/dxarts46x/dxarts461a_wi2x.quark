/*
Title: 02-type-2-synthesis

Course: DXARTS 461a Winter 2023 (https://canvas.uw.edu/courses/1612218)
Authors: Joseph Anderson, Dan Peterson, James Wenlock, et al.
Affiliation: DXARTS, University of Washington (https://dxarts.washington.edu/)
License: GPLv3

*/

/*
Main Paradigm example:

04a-AM-Modulation
-->Type 2 Synthesis


To render, evaluate the code block found in "02-type-2-synthesis.scd".

*/