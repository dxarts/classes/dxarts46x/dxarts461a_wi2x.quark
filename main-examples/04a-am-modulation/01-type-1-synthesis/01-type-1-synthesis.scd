/*
Title: 01-type-1-synthesis

Course: DXARTS 461a Winter 2023 (https://canvas.uw.edu/courses/1612218)
Authors: Joseph Anderson, Dan Peterson, James Wenlock, et al.
Affiliation: DXARTS, University of Washington (https://dxarts.washington.edu/)
License: GPLv3

*/

/*
Evaluate this code block to render.
*/
(
var main;

main = MainParadigm("".resolveRelative, s, true);


///////////////// POPULATE THE SCORE //////////////////

// first: play a "scale", changing modulation index
main.functions[\amIndexScale].value(
	main,
	start: 0.0,
	dur: 2.0,  // "note" duration
	freq0: 440.0,  // carrier
	freq1: 220.0,  // difference
	modIndexGains: Array.series(6, -18.0, 9.0)  // dB: [ -18.0, -9.0, 0.0, 9.0, 18.0, 27.0 ]
);


// next: play a "scale", changing ratio between carrier and modulator
main.functions[\amFreqScale].value(
	main,
	start: 14.0,
	dur: 2.0,
	freq0: 220.0,  // carrier
	// freq1: -440.0,  // difference
	freq1: Array.geom(7, -220, 2.pow(6.reciprocal)).round(0.1),  // freqDiff: [ -220.0, -246.9, -277.2, -311.1, -349.2, -392.0, -440.0 ]
	modIndexGain: -9.0
);

main.render(fileName: "01-type-1-synthesis");
)
