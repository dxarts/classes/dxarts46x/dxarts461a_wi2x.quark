/*
Title: 01-sinosc-without-an-envelope

Course: DXARTS 461a Winter 2023 (https://canvas.uw.edu/courses/1612218)
Authors: Joseph Anderson, Dan Peterson, James Wenlock, et al.
Affiliation: DXARTS, University of Washington (https://dxarts.washington.edu/)
License: GPLv3
*/

/*
A Sine Oscillator UGen graph function

reference by: \sinOscNoEnv
*/
({ |main|
	{ |gain, freq|

		// variables
		var bus;          // var to specify output bus
		var osc, out;     // vars assigned to audio signals
		var amp, phase;  // a few vars for synthesis

		// assign values
		bus = 0;          // first output
		phase = 0;        // phase of oscillator

		// calcs
		amp = gain.dbamp; // convert from gain in dB to linear amplitude scale

		// the oscillator
		osc = SinOsc.ar( // SinOsc UGen
			freq,        // 1st arg of SinOsc: freq (from synthDef argument)
			phase,       // 2nd arg of SinOsc: (set to a default value)
			amp          // 3rd arg of SinOsc: (set to a default value)
		);

		// expand to two channels
		out = [osc, osc];

		// out!!
		Out.ar(         // Out UGen
			bus,        // 1st arg of Out: (set to a default value - the 1st output)
			out         // 2nd arg of Out: (two channels)
		)
	}
})
