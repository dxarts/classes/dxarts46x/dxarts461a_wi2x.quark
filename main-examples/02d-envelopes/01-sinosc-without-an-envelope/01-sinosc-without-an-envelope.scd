/*
Title: 01-sinosc-without-an-envelope

Course: DXARTS 461a Winter 2023 (https://canvas.uw.edu/courses/1612218)
Authors: Joseph Anderson, Dan Peterson, James Wenlock, et al.
Affiliation: DXARTS, University of Washington (https://dxarts.washington.edu/)
License: GPLv3

*/

/*
Evaluate this code block to render.
*/
(
var main;

main = MainParadigm("".resolveRelative, s, true);

main.score.add(
	main.synthDefs[\sinOscNoEnv]
	.note(starttime: 0, duration: 2)
	.gain_(-12)
	.freq_(440)
);

main.render(fileName: "01-sinosc-without-an-envelope");
)
