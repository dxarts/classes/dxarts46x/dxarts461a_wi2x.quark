/*
Title: 01-sinosc-without-an-envelope

Course: DXARTS 461a Winter 2023 (https://canvas.uw.edu/courses/1612218)
Authors: Joseph Anderson, Dan Peterson, James Wenlock, et al.
Affiliation: DXARTS, University of Washington (https://dxarts.washington.edu/)
License: GPLv3

*/

/*
Main Paradigm example:

02d-Envelopes
-->SinOsc without an Envelope


To render, evaluate the code block found in "01-sinosc-without-an-envelope.scd".
*/
