/*
Title: 03-sinosc-with-an-envelope-xline

Course: DXARTS 461a Winter 2023 (https://canvas.uw.edu/courses/1612218)
Authors: Joseph Anderson, Dan Peterson, James Wenlock, et al.
Affiliation: DXARTS, University of Washington (https://dxarts.washington.edu/)
License: GPLv3

*/

/*
Main Paradigm example:

02d-Envelopes
-->SinOsc with an Envelope: XLine


To render, evaluate the code block found in "03-sinosc-with-an-envelope-xline.scd".
*/
