/*
Course: DXARTS 461a Winter 2023 (https://canvas.uw.edu/courses/1612218)
Authors: Joseph Anderson, Dan Peterson, James Wenlock, et al.
Affiliation: DXARTS, University of Washington (https://dxarts.washington.edu/)
License: GPLv3

---- sine law
advocated by Moore
​
a function to return time delays for Sine Time Difference Panning Law
position argument in radians

*/
({ |angle = 0, maxDelay = 0.0006|
    var theta, sinTheta;

    theta = 2 * angle;
    sinTheta = theta.sin;

    maxDelay/2  * ([-1, 1] * sinTheta + sinTheta.abs)
})
