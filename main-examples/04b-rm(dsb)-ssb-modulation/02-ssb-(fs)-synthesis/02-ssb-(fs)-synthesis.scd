/*
Title: 02-ssb-(fs)-synthesis

Course: DXARTS 461a Winter 2023 (https://canvas.uw.edu/courses/1612218)
Authors: Joseph Anderson, Dan Peterson, James Wenlock, et al.
Affiliation: DXARTS, University of Washington (https://dxarts.washington.edu/)
License: GPLv3

*/

/*
Evaluate this code block to render.
*/
(
var main;
var size, harmonicQuadBufs;

main = MainParadigm("".resolveRelative, s, true);

///////////////// DESIGN WAVETABLE BUFFERS (& ADD TO SCORE) //////////////////

size = 4096;  // wavetable size


// harmonic quadrature buffers, an array [ real, imag ]
harmonicQuadBufs = main.functions[\makeQuadBufs].value(
	main,
	size,
	main.functions[\partialGains].value  // multiple gains --> harmonic sinusoids
);


///////////////// POPULATE THE SCORE //////////////////


// next: play a "scale", changing ratio between carrier and modulator
// carrier on harmonic series: 220.0 Hz
main.functions[\ssbFreqScale].value(
	main,
	start: 0.0,
	dur: 2.0,
	freqCar: Array.series(7, 0, 220.0), // [ 0.0, 220.0, 440.0, 660.0, 880.0, 1100.0, 1320.0 ], carrier frequencies
	freqMod: 220.0,  // modulator
	buffers: harmonicQuadBufs  // <--- buffers!!
);

// next: play a "scale", changing ratio between carrier and modulator
// carrier on odd partials of harmonic series: 110.0 Hz, octave below carrier
main.functions[\ssbFreqScale].value(
	main,
	start: 16.0,
	dur: 2.0,
	freqCar: Array.series(7, 110.0, 220.0), // [ 110.0, 330.0, 550.0, 770.0, 990.0, 1210.0, 1430.0 ], carrier frequencies
	freqMod: 220.0,  // modulator
	buffers: harmonicQuadBufs  // <--- buffers!!
);

// next: play a "scale", changing ratio between lowest and next partial (f0, f1)
// spectrum becomes "stretched"
main.functions[\ssbStretchScale].value(
	main,
	start: 32.0,
	dur: 2.0,
	freq: 220.0,  // lowest partial freq, "fundamental"
	ratios:  Array.geom(7, 2, (3/2).pow(1/6)),  // [ 2.0, 2.1, 2.3, 2.4, 2.6, 2.8, 3.0 ], ratios: f1/f0
	buffers: harmonicQuadBufs  // <--- buffers!!
);

// next: play a "scale", changing ratio between lowest and next partial (f0, f1)
// spectrum becomes "compressed"
main.functions[\ssbStretchScale].value(
	main,
	start: 48.0,
	dur: 2.0,
	freq: 220.0,  // lowest partial freq, "fundamental"
	ratios:  Array.geom(7, 2, (3/4).pow(1/6)),  // [ 2.0, 1.9, 1.8, 1.7, 1.7, 1.6, 1.5 ], ratios: f1/f0
	buffers: harmonicQuadBufs  // <--- buffers!!
);

main.render(fileName: "02-ssb-(fs)-synthesis");
)
