/*
Title: 01-granular-density

Course: DXARTS 461a Winter 2023 (https://canvas.uw.edu/courses/1612218)
Authors: Joseph Anderson, Dan Peterson, James Wenlock, et al.
Affiliation: DXARTS, University of Washington (https://dxarts.washington.edu/)
License: GPLv3

*/

/*
Evaluate this code block to render.
*/
(
var main;

main = MainParadigm("".resolveRelative, s, true);


///////////////// EVALUATE FUNCTIONS //////////////////

// evaluate simple scale function
// vary density - constant formant, constant Q
main.functions[\densityScale].value(
	main: main,
	start: 0.0,
	dur: 2.0,
	gain: -9.0,
	ris: 0.1,
	dec: 0.1,
	density: 1.0,
	formFreq: 609.0,
	q: 1.0,
);


// evaluate formant scale function
// vary formant - constant fundamental, constant Q
main.functions[\formantScale].value(
	main: main,
	start: 12.0,
	dur: 1.0,
	gain: -9.0,
	ris: 0.1,
	dec: 0.1,
	density: 1.0,
	formFreq: 609.0,
	q: 1.0,
);


// evaluate formant scale function
// vary Q - constant fundamental, constant formant
main.functions[\qScale].value(
	main: main,
	start: 24.0,
	dur: 1.0,
	gain: -9.0,
	ris: 0.1,
	dec: 0.1,
	density: 1.0,
	formFreq: 609.0,
	q: 1.0,
);


main.render(fileName: "01-granular-density");
)
