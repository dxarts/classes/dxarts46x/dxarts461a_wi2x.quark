/*
Course: DXARTS 461a Winter 2023 (https://canvas.uw.edu/courses/1612218)
Authors: Joseph Anderson, Dan Peterson, James Wenlock, et al.
Affiliation: DXARTS, University of Washington (https://dxarts.washington.edu/)
License: GPLv3

Colored Noise Synthesis

Color: White
Fall Off: 0dB / octave

*/

({ |main|
	{| dur, gain = -12.0, ris = 0.1, dec = 0.1, panAngle = 0.0|

		// variables
		var bus;          // var to specify output bus
		var noise, out;     // vars assigned to audio signals
		var amp;  // a few vars for synthesis
		var ampEnv, env;       // vars for envelope signal

		// assign values
		bus = 0;          // first output

		// calcs
		amp = gain.dbamp;  // convert from gain in dB to linear amplitude scale

		// the amplitude envelope
		env = Env.linen(ris, 1.0 - (ris + dec), dec);

		// the UGen that synthesises the envelope
		ampEnv = EnvGen.kr(env, timeScale: dur);


		// colored noise
		noise = WhiteNoise.ar(amp);

		// rescale noise, by multiplying by ampEnv
		noise = ampEnv * noise;

		// expand to two channels - panning
		out = main.functions[\sinCosPanLaw].value(panAngle) * noise;  // <-- Panning happens here!

		// out!!
		Out.ar(bus, out)
	}
})
