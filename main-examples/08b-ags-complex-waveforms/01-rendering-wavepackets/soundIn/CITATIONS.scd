/*
Course: DXARTS 461a Winter 2023 (https://canvas.uw.edu/courses/1612218)
Authors: Joseph Anderson, Dan Peterson, James Wenlock, et al.
Affiliation: DXARTS, University of Washington (https://dxarts.washington.edu/)
License: GPLv3

*/

/*
Attribution:

low.wav

Downloaded on April 3rd, 2014
Soprano
Performer: Kristen Butchatsky
Technicians: UNKNOWN
http://newt.phys.unsw.edu.au/jw/sounds/singers/low.wav
License: UNKNOWN

See also:
http://newt.phys.unsw.edu.au/jw/soprane.html
http://newt.phys.unsw.edu.au/music/
*/